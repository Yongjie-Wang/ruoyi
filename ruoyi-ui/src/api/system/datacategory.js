import request from '@/utils/request'

// 查询数据分类列表
export function listDatacategory(query) {
  return request({
    url: '/system/datacategory/list',
    method: 'get',
    params: query
  })
}

// 查询数据分类详细
export function getDatacategory(id) {
  return request({
    url: '/system/datacategory/' + id,
    method: 'get'
  })
}

// 新增数据分类
export function addDatacategory(data) {
  return request({
    url: '/system/datacategory',
    method: 'post',
    data: data
  })
}

// 修改数据分类
export function updateDatacategory(data) {
  return request({
    url: '/system/datacategory',
    method: 'put',
    data: data
  })
}

// 删除数据分类
export function delDatacategory(id) {
  return request({
    url: '/system/datacategory/' + id,
    method: 'delete'
  })
}
// 查询listTreeNode
export function listTreeNode(query) {
  return request({
    url: '/system/datacategory/listTreeNode',
    method: 'get',
    params: query
  })
}