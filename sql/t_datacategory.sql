use `ry-vue`;
DROP TABLE IF EXISTS t_datacategory;

CREATE TABLE t_datacategory (
                                id varchar(30) NOT NULL COMMENT 'ID',
                                parent_id varchar(30) NULL DEFAULT '0' COMMENT '父节点',
                                name varchar(60) NOT NULL COMMENT '分类名称',
                                type varchar(60) NULL COMMENT '类型',
                                seq int NULL DEFAULT '0' COMMENT '排序号',
                                remark text NULL COMMENT '备注',
                                img varchar(100) NULL COMMENT '图片',
                                create_at varchar(24) NULL COMMENT '创建日期',
                                create_by varchar(30) NULL COMMENT '创建者',
                                update_at varchar(24) NULL COMMENT '修改日期',
                                update_by varchar(30) NULL COMMENT '修改者',
                                PRIMARY KEY  (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;