package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.TDatacategory;

/**
 * 数据分类Mapper接口
 * 
 * @author ruoyi
 * @date 2023-08-27
 */
public interface TDatacategoryMapper 
{
    /**
     * 查询数据分类
     * 
     * @param id 数据分类主键
     * @return 数据分类
     */
    public TDatacategory selectTDatacategoryById(String id);

    /**
     * 查询数据分类列表
     * 
     * @param tDatacategory 数据分类
     * @return 数据分类集合
     */
    public List<TDatacategory> selectTDatacategoryList(TDatacategory tDatacategory);

    /**
     * 新增数据分类
     * 
     * @param tDatacategory 数据分类
     * @return 结果
     */
    public int insertTDatacategory(TDatacategory tDatacategory);

    /**
     * 修改数据分类
     * 
     * @param tDatacategory 数据分类
     * @return 结果
     */
    public int updateTDatacategory(TDatacategory tDatacategory);

    /**
     * 删除数据分类
     * 
     * @param id 数据分类主键
     * @return 结果
     */
    public int deleteTDatacategoryById(String id);

    /**
     * 批量删除数据分类
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTDatacategoryByIds(String[] ids);
}
