package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 数据分类对象 t_datacategory
 * 
 * @author ruoyi
 * @date 2023-08-27
 */
public class TDatacategory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private String id;

    /** 父节点 */
    @Excel(name = "父节点")
    private String parentId;

    /** 分类名称 */
    @Excel(name = "分类名称")
    private String name;

    /** 类型 */
    @Excel(name = "类型")
    private String type;

    /** 排序号 */
    @Excel(name = "排序号")
    private Long seq;

    /** 图片 */
    @Excel(name = "图片")
    private String img;

    /** 创建日期 */
    @Excel(name = "创建日期")
    private String createAt;

    /** 修改日期 */
    @Excel(name = "修改日期")
    private String updateAt;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setParentId(String parentId) 
    {
        this.parentId = parentId;
    }

    public String getParentId() 
    {
        return parentId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setSeq(Long seq) 
    {
        this.seq = seq;
    }

    public Long getSeq() 
    {
        return seq;
    }
    public void setImg(String img) 
    {
        this.img = img;
    }

    public String getImg() 
    {
        return img;
    }
    public void setCreateAt(String createAt) 
    {
        this.createAt = createAt;
    }

    public String getCreateAt() 
    {
        return createAt;
    }
    public void setUpdateAt(String updateAt) 
    {
        this.updateAt = updateAt;
    }

    public String getUpdateAt() 
    {
        return updateAt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("parentId", getParentId())
            .append("name", getName())
            .append("type", getType())
            .append("seq", getSeq())
            .append("remark", getRemark())
            .append("img", getImg())
            .append("createAt", getCreateAt())
            .append("createBy", getCreateBy())
            .append("updateAt", getUpdateAt())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
