package com.ruoyi.system.domain.vo;

import java.io.Serializable;
import java.util.List;

public class TreeNode implements Serializable {
    private static final long serialVersionUID = 1L;

    /** ID */
    private String id;

    /** 父节点 */

    private String label;

    private List<TreeNode> children;

    public TreeNode(String id, String label, List<TreeNode> children) {
        this.id = id;
        this.label = label;
        this.children = children;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<TreeNode> getChildren() {
        return children;
    }

    public void setChildren(List<TreeNode> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "TreeNode{" +
                "id='" + id + '\'' +
                ", label='" + label + '\'' +
                ", children=" + children +
                '}';
    }
}
