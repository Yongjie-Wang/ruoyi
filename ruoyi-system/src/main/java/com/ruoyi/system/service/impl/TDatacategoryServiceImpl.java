package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ruoyi.system.domain.vo.TreeNode;
import com.ruoyi.system.mapper.TDatacategoryMapper;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.domain.TDatacategory;
import com.ruoyi.system.service.ITDatacategoryService;

/**
 * 数据分类Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-08-27
 */
@Service
public class TDatacategoryServiceImpl implements ITDatacategoryService 
{
    @Autowired
    private TDatacategoryMapper tDatacategoryMapper;

    /**
     * 查询数据分类
     * 
     * @param id 数据分类主键
     * @return 数据分类
     */
    @Override
    public TDatacategory selectTDatacategoryById(String id)
    {
        return tDatacategoryMapper.selectTDatacategoryById(id);
    }

    /**
     * 查询数据分类列表
     * 
     * @param tDatacategory 数据分类
     * @return 数据分类
     */
    @Override
    public List<TDatacategory> selectTDatacategoryList(TDatacategory tDatacategory)
    {
        return tDatacategoryMapper.selectTDatacategoryList(tDatacategory);
    }

    /**
     * 新增数据分类
     * 
     * @param tDatacategory 数据分类
     * @return 结果
     */
    @Override
    public int insertTDatacategory(TDatacategory tDatacategory)
    {
        String randomString = RandomStringUtils.randomAlphanumeric(10);
        tDatacategory.setId(randomString);
        return tDatacategoryMapper.insertTDatacategory(tDatacategory);
    }

    /**
     * 修改数据分类
     * 
     * @param tDatacategory 数据分类
     * @return 结果
     */
    @Override
    public int updateTDatacategory(TDatacategory tDatacategory)
    {
        return tDatacategoryMapper.updateTDatacategory(tDatacategory);
    }

    /**
     * 批量删除数据分类
     * 
     * @param ids 需要删除的数据分类主键
     * @return 结果
     */
    @Override
    public int deleteTDatacategoryByIds(String[] ids)
    {
        return tDatacategoryMapper.deleteTDatacategoryByIds(ids);
    }

    /**
     * 删除数据分类信息
     * 
     * @param id 数据分类主键
     * @return 结果
     */
    @Override
    public int deleteTDatacategoryById(String id)
    {
        return tDatacategoryMapper.deleteTDatacategoryById(id);
    }

    @Override
    public List<TreeNode> listTreeNode() {
        List<TreeNode> treeNodes = new ArrayList<>();
        Map<String, TreeNode> map = new HashMap<>();
        List<TDatacategory> tDatacategories = selectTDatacategoryList(null);

        // 第一次遍历，创建根节点并保存到 map 中
        tDatacategories.forEach(item -> {
            if (item.getParentId().equals("0")) {
                TreeNode treeNode = new TreeNode(item.getId(), item.getName(), new ArrayList<>());
                map.put(item.getId(), treeNode);
            }
        });

        // 第二次遍历，构建树结构
        tDatacategories.forEach(item -> {
            if (!item.getParentId().equals("0")) {
                String parentId = item.getParentId();
                if (map.containsKey(parentId)) {
                    TreeNode parent = map.get(parentId);
                    TreeNode subNode = new TreeNode(item.getId(), item.getName(), new ArrayList<>());
                    parent.getChildren().add(subNode);
                    map.put(item.getId(), subNode);
                }
            }
        });

        // 将根节点添加到结果列表中
        for (String rootNodeId : map.keySet()) {
            TreeNode rootNode = map.get(rootNodeId);
            treeNodes.add(rootNode);
        }

        return treeNodes;
    }
}
