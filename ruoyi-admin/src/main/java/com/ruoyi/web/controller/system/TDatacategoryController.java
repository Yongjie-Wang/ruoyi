package com.ruoyi.web.controller.system;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.system.domain.vo.TreeNode;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.TDatacategory;
import com.ruoyi.system.service.ITDatacategoryService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 数据分类Controller
 * 
 * @author ruoyi
 * @date 2023-08-27
 */
@RestController
@RequestMapping("/system/datacategory")
public class TDatacategoryController extends BaseController
{
    @Autowired
    private ITDatacategoryService tDatacategoryService;

    /**
     * 查询数据分类列表
     */
    @PreAuthorize("@ss.hasPermi('system:datacategory:list')")
    @GetMapping("/list")
    public TableDataInfo list(TDatacategory tDatacategory)
    {
        startPage();
        List<TDatacategory> list = tDatacategoryService.selectTDatacategoryList(tDatacategory);
        return getDataTable(list);
    }

    /**
     * 导出数据分类列表
     */
    @PreAuthorize("@ss.hasPermi('system:datacategory:export')")
    @Log(title = "数据分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TDatacategory tDatacategory)
    {
        List<TDatacategory> list = tDatacategoryService.selectTDatacategoryList(tDatacategory);
        ExcelUtil<TDatacategory> util = new ExcelUtil<TDatacategory>(TDatacategory.class);
        util.exportExcel(response, list, "数据分类数据");
    }

    /**
     * 获取数据分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:datacategory:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return success(tDatacategoryService.selectTDatacategoryById(id));
    }

    /**
     * 新增数据分类
     */
    @PreAuthorize("@ss.hasPermi('system:datacategory:add')")
    @Log(title = "数据分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TDatacategory tDatacategory)
    {
        return toAjax(tDatacategoryService.insertTDatacategory(tDatacategory));
    }

    /**
     * 修改数据分类
     */
    @PreAuthorize("@ss.hasPermi('system:datacategory:edit')")
    @Log(title = "数据分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TDatacategory tDatacategory)
    {
        return toAjax(tDatacategoryService.updateTDatacategory(tDatacategory));
    }

    /**
     * 删除数据分类
     */
    @PreAuthorize("@ss.hasPermi('system:datacategory:remove')")
    @Log(title = "数据分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tDatacategoryService.deleteTDatacategoryByIds(ids));
    }
    /**
     * 查询树形列表
     */
    @GetMapping("/listTreeNode")
    public AjaxResult listTreeNode()
    {
        return success(tDatacategoryService.listTreeNode());
    }
}
